import CartParser from './CartParser';
import path from 'path';
let parser;
const filePath = path.resolve(__dirname, '..', 'samples', 'cart.csv');
const mockPath = path.resolve(__dirname, '..', 'samples', 'mockCart.csv');

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {

	it('read order csv file', () => {
		expect(parser.readFile(filePath)).toEqual(expect.anything());
	});

	describe('validation function', () => {
		const data = `Product name, Price, Quantity
									First, 				 20, 		2 
									Second, 			 15, 		1`;
		const expected = {
			type: expect.any(String),
			row: expect.any(Number),
			column: expect.any(Number),
			message: expect.any(String),
		}

		it('success validation returns empty array', () => {
			expect(parser.validate(data)).toEqual([]);
		});

		it('validation error when check wrond csv file', () => {
			const msg = parser.validate(mockPath);
			msg.map(el => expect(el).toStrictEqual(
				expect.objectContaining(expected)
			));
		});

		it('validation error when cell is NaN', () => {
			[].map(el => expect(el).toStrictEqual(
				expect.objectContaining(expected)
			));
		});

	});

	it('line parser of each line should return template object', () => {
		const mockLine = " First, 10, 4 ";
		const expected = {
			name: expect.any(String),
			price: expect.any(Number),
			quantity: expect.any(Number),
			id: expect.any(String)
		}
		expect(parser.parseLine(mockLine)).toEqual(expect.objectContaining(expected));
	});

	it('calculate total summ of order', () => {
		const mockData = [{
				price: 10,
				quantity: 2
			},
			{
				price: 40,
				quantity: 2
			}
		];
		expect(parser.calcTotal(mockData)).toStrictEqual(expect.any(Number));
		expect(parser.calcTotal(mockData)).not.toBeNaN()
	});

	it('error form should be equal to template', () => {
		const expected = {
			type: expect.any(String),
			row: expect.any(Number),
			column: expect.any(Number),
			message: expect.any(String),
		}
		expect(parser.createError('error', 1, 2, 'message')).
		toStrictEqual(expect.objectContaining(expected))
	});
});


describe('CartParser - integration test', () => {

	it('check all class with csv order file', () => {
		const expected = {
			items: expect.any(Array),
			total: expect.any(Number),
		}
		expect(parser.parse(filePath)).toStrictEqual(
			expect.objectContaining(expected)
		);
	});

	it('parse function error', () => {
		expect(() => parser.parse(mockPath)).toThrow('Validation failed!');
	});

});